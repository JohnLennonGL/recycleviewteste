package com.jlngls.recycleviewteste.Activity.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.jlngls.recycleviewteste.Activity.Adapter.Adapter;
import com.jlngls.recycleviewteste.Activity.Model.ModelFilme;
import com.jlngls.recycleviewteste.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private RecyclerView recyclerView;


    //===========================================================================================================================
    // criando uma listaFilme com modelo do construtor modelFilme.
    private List<ModelFilme> listaFilme = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerViewID);

        //============================================================================================================================
        // configurar RecycleView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        //============================================================================================================================
        //configurar Adapter
        Adapter adapter = new Adapter(listaFilme);
        recyclerView.setAdapter(adapter);



    }

    //============================================================================================================================
    //criando metodo criarFilmes

    public void criarFilmes(){
      ModelFilme filme = new ModelFilme("Titulo","Ano","genero");
      //adicionando mais um filme a lista filme
      this.listaFilme.add(filme);
    }


}
