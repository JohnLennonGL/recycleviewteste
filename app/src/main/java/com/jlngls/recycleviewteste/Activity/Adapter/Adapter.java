package com.jlngls.recycleviewteste.Activity.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.recycleviewteste.Activity.Model.ModelFilme;
import com.jlngls.recycleviewteste.Activity.MyViewHolder.MyViewHolder;
import com.jlngls.recycleviewteste.R;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<MyViewHolder> {

    private List<ModelFilme> listaFilme;

    public Adapter(List<ModelFilme> lista) {
        this.listaFilme = lista;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_list_adapter,parent,false);
        return  new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        listaFilme.get(position);
        ModelFilme filme = listaFilme.get(position);
        holder.ano.setText(filme.getAno());
        holder.genero.setText(filme.getGenero());
        holder.titulo.setText(filme.getTituloFilme());
    }

    @Override
    public int getItemCount() {
        return listaFilme.size();
    }
}


