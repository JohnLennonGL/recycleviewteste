package com.jlngls.recycleviewteste.Activity.MyViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.recycleviewteste.R;

import java.text.BreakIterator;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView ano;
    public TextView genero;
    public TextView titulo;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        titulo = itemView.findViewById(R.id.tituloID);
        ano = itemView.findViewById(R.id.anoID);
        genero = itemView.findViewById(R.id.generoID);
    }
}
