package com.jlngls.recycleviewteste.Activity.Model;

public class ModelFilme {

    private String tituloFilme;
    private String ano;
    private  String genero;

    //============================================================================================================================
    // Configurar construtor com parametros - vá em  generater > construtor e selecione as Variaveis que voce criou
        public ModelFilme(String tituloFilme, String ano, String genero) {
        this.tituloFilme = tituloFilme;
        this.ano = ano;
        this.genero = genero;
    }

    //============================================================================================================================
    // Configurar construtor sem parametros - vá em generater > getter  e selecione as Variaveis que voce criou

    public ModelFilme(){

    }

    public String getTituloFilme() {
        return tituloFilme;
    }

    public String getAno() {
        return ano;
    }

    public String getGenero() {
        return genero;
    }


}
